package com.udimi.dzzirt.udimitest.data.backend.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProfileIdentifier implements Serializable {

    @SerializedName("id")
    long id;

    public ProfileIdentifier(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }
}
