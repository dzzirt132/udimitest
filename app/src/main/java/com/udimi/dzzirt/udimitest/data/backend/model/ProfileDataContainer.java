package com.udimi.dzzirt.udimitest.data.backend.model;

import com.google.gson.annotations.SerializedName;

public class ProfileDataContainer {
    @SerializedName("data")
    ProfileContainer profileContainer;

    public ProfileContainer getProfileContainer() {
        return profileContainer;
    }
}
