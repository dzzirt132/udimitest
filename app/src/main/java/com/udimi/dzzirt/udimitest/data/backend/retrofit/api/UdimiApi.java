package com.udimi.dzzirt.udimitest.data.backend.retrofit.api;

import com.udimi.dzzirt.udimitest.data.backend.model.FiltersCountainer;
import com.udimi.dzzirt.udimitest.data.backend.model.FiltersDataContainer;
import com.udimi.dzzirt.udimitest.data.backend.model.ProfileDataContainer;
import com.udimi.dzzirt.udimitest.data.backend.model.ProfileIdentifier;
import com.udimi.dzzirt.udimitest.data.backend.model.Profile;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UdimiApi {
    @POST("profile/buyOptions")
    Single<FiltersDataContainer> getFiltersContainer(@Body ProfileIdentifier profileIdentifier);

    @POST("profile/")
    Single<ProfileDataContainer> getSellerProfile(@Body ProfileIdentifier profileIdentifier);
}
