package com.udimi.dzzirt.udimitest.data.backend.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Profile implements Serializable {

    public interface Property {
        String NAME = "partner";
        String SUCCESS_RATE = "seller_rate_success";
        String FAIL_RATE = "seller_rate_fail";
        String SALES_PERCENT = "sales_percent";
        String AVATAR_URL = "avatar";
        String IS_ONLINE = "is_online";
        String COUNTRY = "country_name";
        String SOLOS = "profileSolos";
    }

    @SerializedName(Property.NAME)
    String name;

    @SerializedName(Property.SUCCESS_RATE)
    String successRate;

    @SerializedName(Property.FAIL_RATE)
    String failRate;

    @SerializedName(Property.SALES_PERCENT)
    int salesPercent;

    @SerializedName(Property.AVATAR_URL)
    String avatarUrl;

    @SerializedName(Property.IS_ONLINE)
    int isOnline;

    @SerializedName(Property.COUNTRY)
    String country;

    @SerializedName(Property.SOLOS)
    Solos solos;


    public String getName() {
        return name;
    }

    public String getSuccessRate() {
        return successRate;
    }

    public String getFailRate() {
        return failRate;
    }

    public int getSalesPercent() {
        return salesPercent;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public boolean isOnline() {
        return isOnline == 1;
    }

    public String getCountry() {
        return country;
    }

    public Solos getSolos() {
        return solos;
    }
}
