package com.udimi.dzzirt.udimitest.mvp.main;

import com.arellomobile.mvp.MvpView;

public interface ProfileView extends MvpView {
    void setName(String name);

    void setCountry(String country);

    void setSuccessRate(String successRate);

    void setFailRate(String failRate);

    void setHotSold(int hotSold);

    void setOnline();

    void setOffline();

    void setSailsPercent(int sailsPercent);

    void showAvatar(String url);

    void setTotalPrice(long price);

    void setVisitorsCount(int visitorsCount);

    void showError(String error);
}
