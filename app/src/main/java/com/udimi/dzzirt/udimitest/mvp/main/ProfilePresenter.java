package com.udimi.dzzirt.udimitest.mvp.main;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.udimi.dzzirt.udimitest.UdimiTestApplication;
import com.udimi.dzzirt.udimitest.data.backend.BackendProvider;
import com.udimi.dzzirt.udimitest.data.backend.model.Filter;
import com.udimi.dzzirt.udimitest.data.backend.model.Profile;
import com.udimi.dzzirt.udimitest.data.backend.model.ProfileIdentifier;
import com.udimi.dzzirt.udimitest.utils.NetworkUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class ProfilePresenter extends MvpPresenter<ProfileView>
{
    private static final int VISITORS_SHIFT = 50;
    private static final String UNAVAILABLE_URL_ERROR = "Please provide available web address";

    public interface FilterType {

        String ONLY_MOBILE = "Only mobile";

        String NO_MOBILE = "No mobile";
        String ONLY_TOP_TIER = "Only top tier";
        String PRIME_FILTER = "Prime filter";
    }

    @Inject
    BackendProvider backendProvider;

    private Map<String, Double> filtersPrice = new HashMap<>();

    private double filterSum;

    private int visitorsCount = VISITORS_SHIFT;

    private double clickPrice;

    private String url;

    public ProfilePresenter() {
        UdimiTestApplication.getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        ProfileIdentifier profileIdentifier = new ProfileIdentifier(9124);

        backendProvider.getFilterList(profileIdentifier)
                .doOnSuccess(filters -> {
                    initFilterPrices(filters);
                })
                .flatMap(filters -> backendProvider.getProfile(profileIdentifier))
                .subscribe(profile -> {
                    initProfileScreen(profile);
                }, error -> getViewState().showError(error.getMessage()));

    }

    private void initFilterPrices(List<Filter> filters) {
        for (Filter filter : filters) {
            filtersPrice.put(filter.getName(), filter.getPrice());
        }
    }

    public void onVisitorsChanged(int visitorsMultiplier) {
        visitorsCount = visitorsMultiplier * VISITORS_SHIFT;
        getViewState().setVisitorsCount(visitorsCount);

        recalculateTotalPrice();
    }

    public void onNoMobileChanged(boolean selected) {
        Double price = filtersPrice.get(FilterType.NO_MOBILE);
        filterSum += selected ? price : -price;

        recalculateTotalPrice();
    }

    public void onMobileOnlyChanged(boolean selected) {
        Double price = filtersPrice.get(FilterType.ONLY_MOBILE);
        filterSum += selected ? price : -price;

        recalculateTotalPrice();
    }

    public void onTopTierOnlyChanged(boolean selected) {
        Double price = filtersPrice.get(FilterType.ONLY_TOP_TIER);
        filterSum += selected ? price : -price;

        recalculateTotalPrice();
    }

    public void onPrimeFilterChanged(boolean selected) {
        Double price = filtersPrice.get(FilterType.PRIME_FILTER);
        filterSum += selected ? price : -price;

        recalculateTotalPrice();
    }

    private void initProfileScreen(Profile profile) {
        getViewState().setName(profile.getName());
        getViewState().setCountry(profile.getCountry());
        getViewState().setSuccessRate(profile.getSuccessRate());
        getViewState().setFailRate(profile.getFailRate());
        getViewState().setHotSold(Integer.parseInt(profile.getSolos().getHotSalesCount()));
        getViewState().setSailsPercent(profile.getSalesPercent());
        getViewState().showAvatar(profile.getAvatarUrl());
        if (profile.isOnline()) {
            getViewState().setOnline();
        } else {
            getViewState().setOffline();
        }

        clickPrice = profile.getSolos().getClickPrice();
        recalculateTotalPrice();
    }

    private void recalculateTotalPrice() {
        long visitorsPrice = Math.round(clickPrice * visitorsCount);
        long filtersPrice = Math.round(filterSum * visitorsCount);

        getViewState().setTotalPrice((long) Math.ceil(visitorsPrice + filtersPrice + 3));
    }

    public void onUrlChanged(String newUrl) {
        url = newUrl;
    }

    public void onAddToCartClick() {
        Single.fromCallable(() -> NetworkUtils.checkForAvailability(url))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(isAvailable -> {
                    if (!isAvailable) {
                        getViewState().showError(UNAVAILABLE_URL_ERROR);
                    }
                }, error -> getViewState().showError(error.getMessage()));
    }
}
