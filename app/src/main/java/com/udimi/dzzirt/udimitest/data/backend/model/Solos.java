package com.udimi.dzzirt.udimitest.data.backend.model;

import com.google.gson.annotations.SerializedName;

public class Solos {
    public interface Property {
        String CLICK_PRICE = "click_price";
        String HOT_SALES_COUNT = "hot_sales_cnt";
    }

    @SerializedName(Property.HOT_SALES_COUNT)
    String hotSalesCount;

    @SerializedName(Property.CLICK_PRICE)
    double clickPrice;

    public String getHotSalesCount() {
        return hotSalesCount;
    }

    public double getClickPrice() {
        return clickPrice;
    }
}
