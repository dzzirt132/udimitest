package com.udimi.dzzirt.udimitest.data.backend.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FiltersCountainer {
    @SerializedName("filterPriceOptions")
    List<Filter> filters;

    public List<Filter> getFilters() {
        return filters;
    }
}
