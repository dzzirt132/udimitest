package com.udimi.dzzirt.udimitest.data.backend;

import com.udimi.dzzirt.udimitest.data.backend.model.Filter;
import com.udimi.dzzirt.udimitest.data.backend.model.FiltersContainer;
import com.udimi.dzzirt.udimitest.data.backend.model.FiltersDataContainer;
import com.udimi.dzzirt.udimitest.data.backend.model.Profile;
import com.udimi.dzzirt.udimitest.data.backend.model.ProfileContainer;
import com.udimi.dzzirt.udimitest.data.backend.model.ProfileDataContainer;
import com.udimi.dzzirt.udimitest.data.backend.model.ProfileIdentifier;
import com.udimi.dzzirt.udimitest.data.backend.retrofit.api.UdimiApi;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class BackendProvider {

    private UdimiApi udimiApi;

    public BackendProvider(UdimiApi udimiApi) {
        this.udimiApi = udimiApi;
    }

    public Single<Profile> getProfile(ProfileIdentifier profileIdentifier) {
        return udimiApi.getSellerProfile(profileIdentifier)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(ProfileDataContainer::getProfileContainer)
                .map(ProfileContainer::getProfile);

    }

    public Single<List<Filter>> getFilterList(ProfileIdentifier profileIdentifier) {
        return udimiApi.getFiltersContainer(profileIdentifier)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(FiltersDataContainer::getFiltersContainer)
                .map(FiltersContainer::getFilters)
                ;
    }

}
