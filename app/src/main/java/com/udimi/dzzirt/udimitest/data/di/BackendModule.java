package com.udimi.dzzirt.udimitest.data.di;

import com.udimi.dzzirt.udimitest.data.backend.BackendProvider;
import com.udimi.dzzirt.udimitest.data.backend.retrofit.api.UdimiApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class BackendModule {
    @Singleton
    @Provides
    BackendProvider getBackendProvider(UdimiApi udimiApi) {
        return new BackendProvider(udimiApi);
    }
}
