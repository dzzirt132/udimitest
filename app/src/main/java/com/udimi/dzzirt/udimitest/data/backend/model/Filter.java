package com.udimi.dzzirt.udimitest.data.backend.model;

import com.google.gson.annotations.SerializedName;

public class Filter {

    @SerializedName("label")
    String name;

    @SerializedName("price")
    String price;

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return Double.valueOf(price);
    }
}
