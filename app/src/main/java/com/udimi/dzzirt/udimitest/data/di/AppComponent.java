package com.udimi.dzzirt.udimitest.data.di;

import com.udimi.dzzirt.udimitest.mvp.main.ProfilePresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        RetrofitModule.class,
        BackendModule.class})
public interface AppComponent {
    void inject(ProfilePresenter profilePresenter);
}
