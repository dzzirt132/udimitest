package com.udimi.dzzirt.udimitest.data.backend.model;

import com.google.gson.annotations.SerializedName;

public class ProfileContainer {
    @SerializedName("user")
    Profile profile;

    public Profile getProfile() {
        return profile;
    }
}
