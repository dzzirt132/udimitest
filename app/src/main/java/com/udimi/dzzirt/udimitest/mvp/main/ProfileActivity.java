package com.udimi.dzzirt.udimitest.mvp.main;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.udimi.dzzirt.udimitest.R;
import com.udimi.dzzirt.udimitest.utils.ImageUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends MvpAppCompatActivity implements ProfileView, SeekBar.OnSeekBarChangeListener {

    @InjectPresenter
    ProfilePresenter profilePresenter;

    @BindView(R.id.textview_name)
    TextView textViewName;

    @BindView(R.id.textview_country)
    TextView textViewCountry;

    @BindView(R.id.textview_fail_rate)
    TextView textViewFailRate;

    @BindView(R.id.textview_success_rate)
    TextView textViewSuccessRate;

    @BindView(R.id.textview_hot_sold)
    TextView textViewHotSold;

    @BindView(R.id.textview_online)
    TextView textViewOnline;

    @BindView(R.id.textview_sales_percent)
    TextView textViewSalesPercent;

    @BindView(R.id.imageview_avatar)
    ImageView imageViewAvatar;

    @BindView(R.id.seekbar_visitors)
    SeekBar seekBarVisitors;

    @BindView(R.id.textview_visitors)
    TextView textViewVisitors;

    @BindView(R.id.button_add_to_cart)
    Button buttonAddToCart;

    @BindView(R.id.checkbox_no_mobile)
    CheckBox checkBoxNoMobile;

    @BindView(R.id.checkbox_only_mobile)
    CheckBox checkBoxOnlyMobile;

    @BindView(R.id.checkbox_prime_filter)
    CheckBox checkBoxPrimeFilter;

    @BindView(R.id.checkbox_top_tier)
    CheckBox checkBoxTopTier;

    @BindView(R.id.edittext_url)
    EditText editTextUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        seekBarVisitors.setOnSeekBarChangeListener(this);

        checkBoxNoMobile.setOnCheckedChangeListener((buttonView, isChecked) -> {
            profilePresenter.onNoMobileChanged(isChecked);
            checkBoxOnlyMobile.setEnabled(!isChecked);
        });

        checkBoxOnlyMobile.setOnCheckedChangeListener((buttonView, isChecked) -> {
            profilePresenter.onMobileOnlyChanged(isChecked);
            checkBoxNoMobile.setEnabled(!isChecked);
        });

        checkBoxPrimeFilter.setOnCheckedChangeListener((buttonView, isChecked) -> {
            profilePresenter.onPrimeFilterChanged(isChecked);
        });

        checkBoxTopTier.setOnCheckedChangeListener((buttonView, isChecked) -> {
            profilePresenter.onTopTierOnlyChanged(isChecked);
        });

        editTextUrl.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                profilePresenter.onUrlChanged(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void setName(String name) {
        textViewName.setText(name);
    }

    @Override
    public void setCountry(String country) {
        textViewCountry.setText(country);
    }

    @Override
    public void setSuccessRate(String successRate) {
        textViewSuccessRate.setText(successRate);
    }

    @Override
    public void setFailRate(String failRate) {
        textViewFailRate.setText(failRate);
    }

    @Override
    public void setHotSold(int hotSold) {
        textViewHotSold.setText(String.format(getString(R.string.hot_sold), hotSold));
    }

    @Override
    public void setOnline() {
        textViewOnline.setText(R.string.online);
    }

    @Override
    public void setOffline() {
        textViewOnline.setText(R.string.offline);
    }

    @Override
    public void setSailsPercent(int sailsPercent) {
        textViewSalesPercent.setText(String.format(getString(R.string.sales_percent), sailsPercent));
    }

    @Override
    public void showAvatar(String url) {
        ImageUtils.loadCircularFromUrl(url, imageViewAvatar);
    }

    @Override
    public void setTotalPrice(long price) {
        buttonAddToCart.setText(String.format(getString(R.string.add_to_cart), price));
    }

    @Override
    public void setVisitorsCount(int visitorsCount) {
        textViewVisitors.setText(String.format(getString(R.string.visitors_count), visitorsCount));
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        profilePresenter.onVisitorsChanged(progress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // nothing
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        // nothing
    }

    @OnClick(R.id.button_add_to_cart)
    public void onAddToCartClick() {
        profilePresenter.onAddToCartClick();
    }
}
