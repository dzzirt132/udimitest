package com.udimi.dzzirt.udimitest.utils;

import android.webkit.URLUtil;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class NetworkUtils {

    public static boolean checkForAvailability(String url) {
        if (!URLUtil.isValidUrl(url)) {
            return false;
        }

        try {
            URL webUrl = new URL(url);
            URLConnection urlConnection = webUrl.openConnection();
            urlConnection.connect();
        } catch (IOException e) {
            return false;
        }

        return true;
    }
}
