package com.udimi.dzzirt.udimitest.data.backend.model;

import com.google.gson.annotations.SerializedName;

public class FiltersDataContainer {

    @SerializedName("data")
    FiltersContainer filtersContainer;

    public FiltersContainer getFiltersContainer() {
        return filtersContainer;
    }
}
