package com.udimi.dzzirt.udimitest;

import android.app.Application;

import com.udimi.dzzirt.udimitest.data.backend.retrofit.api.RequestInterceptor;
import com.udimi.dzzirt.udimitest.data.di.AppComponent;
import com.udimi.dzzirt.udimitest.data.di.DaggerAppComponent;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UdimiTestApplication extends Application {

    private static final AppComponent appComponent = DaggerAppComponent.create();

    public static AppComponent getAppComponent() {
        return appComponent;
    }

}
