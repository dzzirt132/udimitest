package com.udimi.dzzirt.udimitest.data.backend.retrofit.api;

import com.udimi.dzzirt.udimitest.BuildConfig;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class RequestInterceptor implements Interceptor {
    private static final String AUTH = "Auth";

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request updatedRequest = request.newBuilder()
                .addHeader(AUTH, BuildConfig.AUTH_TOKEN)
                .build();

        return chain.proceed(updatedRequest);
    }
}
